import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by BG on 11-Dec-16.
 */
public class Calculator extends JFrame {

    ScriptEngineManager mgr = new ScriptEngineManager();
    ScriptEngine engine = mgr.getEngineByName("JavaScript");

    private static final Font buttonFont = new Font(Font.SANS_SERIF, Font.BOLD, 22);

    public Calculator() {

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new FlowLayout());
        JPanel screenPanel = new JPanel();
        JPanel buttonPanel = new JPanel(new GridLayout(5,4));
        JLabel label = new JLabel("Developed by BG");

        JTextArea screen = new JTextArea();
        screen.setPreferredSize(new Dimension(190, 50));
        screen.setEditable(false);
        screen.setBackground(Color.BLACK);
        screen.setFont(buttonFont);
        screen.setForeground(Color.WHITE);
        screenPanel.add(screen);

        mainPanel.add(screenPanel);
        mainPanel.add(buttonPanel);
        mainPanel.add(label);

        ImageIcon imageForBksp = new ImageIcon("D:\\BG projects\\Calculator\\src\\main\\resources\\bksp.png");
        JButton bksp = new JButton(imageForBksp);
        bksp.setFont(buttonFont);
        bksp.setBorder(BorderFactory.createEmptyBorder());
        buttonPanel.add(bksp);

        JButton clr = new JButton("C");
        JButton sqrt = new JButton("\u221A");
        JButton percentage = new JButton("%");
        JButton seven = new JButton("7");
        JButton eight = new JButton("8");
        JButton nine = new JButton("9");
        JButton mult = new JButton("*");
        JButton four = new JButton("4");
        JButton five = new JButton("5");
        JButton six = new JButton("6");
        JButton plus = new JButton("+");
        JButton one = new JButton("1");
        JButton two = new JButton("2");
        JButton three = new JButton("3");
        JButton minus = new JButton("-");
        JButton zero = new JButton("0");
        JButton calc = new JButton("=");
        JButton dot = new JButton(".");
        JButton div = new JButton("/");

        JButton[] buttons = {clr, sqrt, percentage, seven, eight,nine, mult, four, five,
                             six, plus, one, two, three, minus, zero, calc, dot, div};


        for (int i=0; i<buttons.length; i++) {
            buttons[i].setFont(buttonFont);
            buttonPanel.add(buttons[i]);
        }

        zero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"0");
            }
        });

        one.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"1");
            }
        });

        two.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"2");
            }
        });

        three.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"3");
            }
        });

        four.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"4");
            }
        });

        five.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"5");
            }
        });

        six.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"6");
            }
        });

        seven.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"7");
            }
        });

        eight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"8");
            }
        });

        nine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"9");
            }
        });

        plus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"+");
            }
        });

        minus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"-");
            }
        });

        mult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"*");
            }
        });

        div.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+"/");
            }
        });

        dot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText()+".");
            }
        });

        clr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText("");
            }
        });

        sqrt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(String.valueOf(Math.sqrt(Double.valueOf(screen.getText()))));
            }
        });

        bksp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screen.setText(screen.getText().substring(0, screen.getText().length()-1));
            }
        });

        calc.addActionListener(new ActionListener (){
            public void actionPerformed(ActionEvent e) {
                String str = screen.getText();
                try {
                    screen.setText(String.valueOf(engine.eval(str)));
                } catch (ScriptException e1) {
                    e1.printStackTrace();
                }
            }
        });

        this.add(mainPanel);
        this.setTitle("Calculator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setResizable(false);
        this.setVisible(true);
        this.setSize(250, 325);
    }

    public void declareButtons() {

    }

    public static void main(String[] argv) {
        Calculator frame = new Calculator();
    }

}
